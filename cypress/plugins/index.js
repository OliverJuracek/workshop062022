/// <reference types="cypress" />
const cucumber = require("cypress-cucumber-preprocessor").default;
const browserify = require("@cypress/browserify-preprocessor");
const path = require('path');
const fs = require('fs-extra');

function getConfigurationByFile(file) {
  const pathToConfigFile = path.resolve('', '', `cypress.${file}.json`)
  return fs.readJson(pathToConfigFile)
}

module.exports = (on, config) => {
  const options = {

    ...browserify.defaultOptions,

    typescript: path.join(path.resolve('.'), 'node_modules/typescript'),

  }
  on("file:preprocessor", cucumber(options));

  const file = config.env.configFile || 'test'

  return getConfigurationByFile(file)
}
