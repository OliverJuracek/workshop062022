export interface DataChangeProposalCreatePage {
    title: () => Cypress.Chainable<JQuery<HTMLElement>>
    labels: () => Cypress.Chainable<JQuery<HTMLElement>>
    inputs: () => Cypress.Chainable<JQuery<HTMLElement>>
    specificInput: (dataCy: string) => Cypress.Chainable<JQuery<HTMLElement>>
    radioButton: (dataCy: string) => Cypress.Chainable<JQuery<HTMLElement>>
    saveButton: () => Cypress.Chainable<JQuery<HTMLElement>>
    cancelButton: () => Cypress.Chainable<JQuery<HTMLElement>>
    inputsRequired: () => Cypress.Chainable<JQuery<HTMLElement>>
    toastType: () => Cypress.Chainable<JQuery<HTMLElement>>
    toastMessage: () => Cypress.Chainable<JQuery<HTMLElement>>
}