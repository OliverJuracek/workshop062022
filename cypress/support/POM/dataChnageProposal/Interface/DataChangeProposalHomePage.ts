export interface DataChangeProposalHomePage {
    title: () => Cypress.Chainable<JQuery<HTMLElement>>
    addButton: () => Cypress.Chainable<JQuery<HTMLElement>>
    list: {
        labels: () => Cypress.Chainable<JQuery<HTMLElement>>
        rows: () => Cypress.Chainable<JQuery<HTMLElement>>
        firstRow: (column: number) => Cypress.Chainable<JQuery<HTMLElement>>
        specificRow: (row: number) => Cypress.Chainable<JQuery<HTMLElement>>
        specificRowAndSubject: (row: number, column: number) => Cypress.Chainable<JQuery<HTMLElement>>
        specificCell: (dataCy: string) => Cypress.Chainable<JQuery<HTMLElement>>
    },
    paginator: {
        option: () => Cypress.Chainable<JQuery<HTMLElement>>
        options: () => Cypress.Chainable<JQuery<HTMLElement>>
        rangeLabel: () => Cypress.Chainable<JQuery<HTMLElement>>
        previousButton: () => Cypress.Chainable<JQuery<HTMLElement>>
        nextButton: () => Cypress.Chainable<JQuery<HTMLElement>>
    },
    spinner: () => Cypress.Chainable<JQuery<HTMLElement>>
    toastType: () => Cypress.Chainable<JQuery<HTMLElement>>
    toastMessage: () => Cypress.Chainable<JQuery<HTMLElement>>
}