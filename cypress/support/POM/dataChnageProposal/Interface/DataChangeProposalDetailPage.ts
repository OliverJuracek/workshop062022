export interface DataChangeProposalDetailPage {
    backButton: () => Cypress.Chainable<JQuery<HTMLElement>>
    title: () => Cypress.Chainable<JQuery<HTMLElement>>
    menuButton: () => Cypress.Chainable<JQuery<HTMLElement>>
    options: (option: string) => Cypress.Chainable<JQuery<HTMLElement>>
    detail: {
        labels: () => Cypress.Chainable<JQuery<HTMLElement>>
        values: () => Cypress.Chainable<JQuery<HTMLElement>>
    }
    input: {
        specificInput: (dataCy: string) => Cypress.Chainable<JQuery<HTMLElement>>
        radioButton: (dataCy: string) => Cypress.Chainable<JQuery<HTMLElement>>
        labels: () => Cypress.Chainable<JQuery<HTMLElement>>
        values: () => Cypress.Chainable<JQuery<HTMLElement>>
    }
    saveButton: () => Cypress.Chainable<JQuery<HTMLElement>>
    cancelButton: () => Cypress.Chainable<JQuery<HTMLElement>>
    tabs: {
        values: () => Cypress.Chainable<JQuery<HTMLElement>>
        events: {
            tabButton: () => Cypress.Chainable<JQuery<HTMLElement>>
            createEventButton: () => Cypress.Chainable<JQuery<HTMLElement>>
            values: () => Cypress.Chainable<JQuery<String>>
            labels: () => Cypress.Chainable<JQuery<HTMLElement>>
            firstRow: (column: number) => Cypress.Chainable<JQuery<HTMLElement>>
            specificRow: (row: number) => Cypress.Chainable<JQuery<HTMLElement>>
            specificRowAndSubject: (row: number, column: number) => Cypress.Chainable<JQuery<HTMLElement>>
            menuButton: () => Cypress.Chainable<JQuery<HTMLElement>>
            menuOptions: (option: string) => Cypress.Chainable<JQuery<HTMLElement>>
        },
        attachments: {
            tabButton: () => Cypress.Chainable<JQuery<HTMLElement>>
            addButton: () => Cypress.Chainable<JQuery<HTMLElement>>
            values: () => Cypress.Chainable<JQuery<String>>
            labels: () => Cypress.Chainable<JQuery<HTMLElement>>
            firstRow: (column: number) => Cypress.Chainable<JQuery<HTMLElement>>
            specificRow: (row: number) => Cypress.Chainable<JQuery<HTMLElement>>
            specificRowAndSubject: (row: number, column: number) => Cypress.Chainable<JQuery<HTMLElement>>
            menuButton: () => Cypress.Chainable<JQuery<HTMLElement>>
            menuOptions: (option: string) => Cypress.Chainable<JQuery<HTMLElement>>
        }
    },
    toastType: () => Cypress.Chainable<JQuery<HTMLElement>>
    toastMessage: () => Cypress.Chainable<JQuery<HTMLElement>>
    modalWindows: {
        event: {
            title: () => Cypress.Chainable<JQuery<HTMLElement>>
            existingEvent: {
                searchButton: () => Cypress.Chainable<JQuery<HTMLElement>>
                value: () => Cypress.Chainable<JQuery<HTMLElement>>
            },
            labels: () => Cypress.Chainable<JQuery<HTMLElement>>
            inputs: () => Cypress.Chainable<JQuery<HTMLElement>>
            inputsRequired: () => Cypress.Chainable<JQuery<HTMLElement>>
            inputScpecific: (inputName: string) => Cypress.Chainable<JQuery<HTMLElement>>
            scenario: {
                dropdown: () => Cypress.Chainable<JQuery<HTMLElement>>
                options: () => Cypress.Chainable<JQuery<HTMLElement>>
            },
            saveButton: () => Cypress.Chainable<JQuery<HTMLElement>>
            cancelButton: () => Cypress.Chainable<JQuery<HTMLElement>>
            progressBar: () => Cypress.Chainable<JQuery<HTMLElement>>
        },
        addAttachment: {
            browseButton: () => Cypress.Chainable<JQuery<HTMLElement>>
            uploadWindow: () => Cypress.Chainable<JQuery<HTMLElement>>
        },
        search: {
            searchButton: () => Cypress.Chainable<JQuery<HTMLElement>>
            inputs: () => Cypress.Chainable<JQuery<HTMLElement>>
            resetButton: () => Cypress.Chainable<JQuery<HTMLElement>>
            firstRow: (column: number) => Cypress.Chainable<JQuery<HTMLElement>>
            specificRow: (row: number) => Cypress.Chainable<JQuery<HTMLElement>>
            specificRowAndSubject: (row: number, column: number) => Cypress.Chainable<JQuery<HTMLElement>>
            selectButton: () => Cypress.Chainable<JQuery<HTMLElement>>
            cancelButton: () => Cypress.Chainable<JQuery<HTMLElement>>
            paginator: {
                rangeLabel: () => Cypress.Chainable<JQuery<HTMLElement>>
                previousButton: () => Cypress.Chainable<JQuery<HTMLElement>>
                nextButton: () => Cypress.Chainable<JQuery<HTMLElement>>
            }
        },
        warning: {
            title: () => Cypress.Chainable<JQuery<HTMLElement>>,
            message: () => Cypress.Chainable<JQuery<String>>,
            okButton: () => Cypress.Chainable<JQuery<HTMLElement>>,
            cancelButton: () => Cypress.Chainable<JQuery<HTMLElement>>,
            closeButton: () => Cypress.Chainable<JQuery<HTMLElement>>,
        }
    }
}