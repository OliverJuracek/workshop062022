import { DataChangeProposalHomePage } from './Interface/DataChangeProposalHomePage'
import { DataChangeProposalDetailPage } from './Interface/DataChangeProposalDetailPage'
import { DataChangeProposalCreatePage } from './Interface/DataChangeProposalCreatePage'

export class DataChangeProposal {
    public dataChangeProposalHome!: DataChangeProposalHomePage;
    public dataChangeProposalDetail!: DataChangeProposalDetailPage;
    public dataChangeProposalCreatePage!: DataChangeProposalCreatePage;

    constructor() {
        this.initDataChangeProposalHomePage()
        this.initDataChangeProposalDetailPage()
        this.initDataChangeProposalCreatePage()
    }

    private initDataChangeProposalHomePage(): void {
        this.dataChangeProposalHome = {
            title: () => cy.get('[data-cy="data-change-proposal-list-title"]'),
            addButton: () => cy.get('[data-cy="data-change-proposal-add-button"]'),
            list: {
                labels: () => cy.get('[data-cy^="table-header"]'),
                rows: () => cy.get('[data-cy^="row-"]'),
                firstRow: (column: number) => cy.get('[data-cy="row-0"] > :nth-child(' + (column + 1) + ')'),
                specificRow: (row: number) => cy.get('[data-cy="row-' + row + '"] > :nth-child(1)'),
                specificRowAndSubject: (row: number, column: number) => cy.get('[data-cy="row-' + row + '"] > :nth-child(' + (column + 1) + ')'),
                specificCell: (dataCy: string) => cy.get('[data-cy="cell-' + dataCy + '"]')
            },
            paginator: {
                option: () => cy.get('mat-select'),
                options: () => cy.get('.mat-option span'),
                rangeLabel: () => cy.get('.mat-paginator-page-size-label'),
                previousButton: () => cy.get('.mat-paginator-navigation-previous').get('.mat-paginator-icon'),
                nextButton: () => cy.get('.mat-paginator-navigation-next'),
            },
            spinner: () => cy.get('.mat-spinner'),
            toastType: () => cy.get('[data-cy="toast-item-severity"]'),
            toastMessage: () => cy.get('[data-cy="toast-item-description"]')
        }
    }

    private initDataChangeProposalCreatePage(): void {
        this.dataChangeProposalCreatePage = {
            title: () => cy.get('header'),
            labels: () => cy.get('[data-cy="label"]'),
            inputs: () => cy.get('[data-cy="input"]'),
            specificInput: (dataCy: string) => cy.get('[data-cy="' + dataCy + '"] [data-cy="input"]'),
            radioButton: (dataCy: string) => cy.get('[data-cy="radio-button-' + dataCy + '"]'),
            saveButton: () => cy.get('[data-cy="save-button"'),
            cancelButton: () => cy.get('[data-cy="cancel-button"'),
            inputsRequired: () => cy.get('[data-cy="required-error"]'),
            toastType: () => cy.get('[data-cy="toast-item-severity"]'),
            toastMessage: () => cy.get('[data-cy="toast-item-description"]'),
        }
    }

    private initDataChangeProposalDetailPage(): void {
        this.dataChangeProposalDetail = {
            backButton: () => cy.get('[data-cy="data-change-proposal-back-button"]'),
            title: () => cy.get('[data-cy="data-change-proposal-title"]').invoke('text'),
            menuButton: () => cy.get('[data-cy="data-change-proposal-actions-button"]'),
            options: (option: string) => cy.get(`[data-cy^="data-change-proposal-action-${option}-button"]`),
            detail: {
                labels: () => cy.get('.proposal-details .label'),
                values: () => cy.get('.proposal-details .value')
            },
            input: {
                specificInput: (dataCy: string) => cy.get('[data-cy="' + dataCy + '"] [data-cy="input"]'),
                radioButton: (dataCy: string) => cy.get('[data-cy="radio-button-' + dataCy + '"]'),
                labels: () => cy.get('[data-cy="label"]'),
                values: () => cy.get('[data-cy="input"]')
            },
            saveButton: () => cy.get('[data-cy="data-change-proposal-save-button"]'),
            cancelButton: () => cy.get('[data-cy="data-change-proposal-cancel-button"]'),
            tabs: {
                values: () => cy.get('[data-cy="tab-header-events"]'),
                events: {
                    tabButton: () => cy.get('#mat-tab-label-0-0'),
                    createEventButton: () => cy.get('[data-cy="data-change-proposal-create-event-button"]'),
                    values: () => cy.get('[data-cy="tab-header-events"]').invoke('text'),
                    labels: () => cy.get('[data-cy^="table-header"]'),
                    firstRow: (column: number) => cy.get('[data-cy="row-0"] > :nth-child(' + (column + 1) + ')'),
                    specificRow: (row: number) => cy.get('[data-cy="row-' + row + '"] > :nth-child(1)'),
                    specificRowAndSubject: (row: number, column: number) => cy.get('[data-cy="row-' + row + '"] > :nth-child(' + (column + 1) + ')'),
                    menuButton: () => cy.get('[data-cy="event-actions-actions-button"]'),
                    menuOptions: (option: string) => cy.get('[data-cy="event-actions-' + option + '-button"]')
                },
                attachments: {
                    tabButton: () => cy.get('#mat-tab-label-0-1'),
                    addButton: () => cy.get('button span').contains('Add attachment'),
                    values: () => cy.get('[data-cy="tab-header-events"]').invoke('text'),
                    labels: () => cy.get('[data-cy^="table-header"]'),
                    firstRow: (column: number) => cy.get('[data-cy="row-0"] > :nth-child(' + (column + 1) + ')'),
                    specificRow: (row: number) => cy.get('[data-cy="row-' + row + '"] > :nth-child(1)'),
                    specificRowAndSubject: (row: number, column: number) => cy.get('[data-cy="row-' + row + '"] > :nth-child(' + (column + 1) + ')'),
                    menuButton: () => cy.get('[data-cy="attachment-actions-actions-button"]'),
                    menuOptions: (option: string) => cy.get('[data-cy="button-' + option + '-attachment"]')
                }
            },
            toastType: () => cy.get('[data-cy="toast-item-severity"]'),
            toastMessage: () => cy.get('[data-cy="toast-item-description"]'),
            modalWindows: {
                event: {
                    title: () => cy.get('cpm-event-detail [data-cy="event-detail-title"]'),
                    existingEvent: {
                        searchButton: () => cy.get('[data-cy="search-existing-event-button"]'),
                        value: () => cy.get('[data-cy="search-existing-event"]')
                    },
                    labels: () => cy.get('cpm-event-detail [data-cy="label"]'),
                    inputs: () => cy.get('cpm-event-detail [data-cy="input"]'),
                    inputsRequired: () => cy.get('[data-cy="required-error"]'),
                    inputScpecific: (inputName: string) => cy.get('cpm-event-detail [data-cy="event-' + inputName + '"] [data-cy="input"]'),
                    scenario: {
                        dropdown: () => cy.get('[data-cy="select"]'),
                        options: () => cy.get('[data-cy^="option-UNGUIDED"]'),
                    },
                    saveButton: () => cy.get('cpm-event-detail [data-cy="event-save-button"]'),
                    cancelButton: () => cy.get('cpm-event-detail [data-cy="event-cancel-button"]'),
                    progressBar: () => cy.get('mat-progress-bar')
                },
                addAttachment: {
                    browseButton: () => cy.get('button span').contains('Browse'),
                    uploadWindow: () => cy.get('adm-file-uploader')
                },
                search: {
                    searchButton: () => cy.get('[data-cy="search-existing-event-button"]'),
                    inputs: () => cy.get('adm-dynamic-form input'),
                    resetButton: () => cy.get('[]'),
                    firstRow: (column: number) => cy.get('[data-cy="row-0"] > :nth-child(' + (column + 1) + ')'),
                    specificRow: (row: number) => cy.get('[data-cy="row-' + row + '"] > :nth-child(1)'),
                    specificRowAndSubject: (row: number, column: number) => cy.get('[data-cy="row-' + row + '"] > :nth-child(' + (column + 1) + ')'),
                    selectButton: () => cy.get('cpm-event-lookup [data-cy="button-select-event"]'),
                    cancelButton: () => cy.get('cpm-event-lookup [data-cy="button-cancel-event"]'),
                    paginator: {
                        rangeLabel: () => cy.get('.mat-paginator-page-size-label'),
                        previousButton: () => cy.get('.mat-paginator-navigation-previous').get('.mat-paginator-icon'),
                        nextButton: () => cy.get('.mat-paginator-navigation-next'),
                    }
                },
                warning: {
                    title: () => cy.get('adm-modal h1 .ng-star-inserted'),
                    message: () => cy.get('adm-modal > div > div > div > span').invoke('text'),
                    okButton: () => cy.get('adm-modal button').contains('ok'),
                    cancelButton: () => cy.get('adm-modal button').contains('Cancel'),
                    closeButton: () => cy.get('adm-modal mat-dialog-close'),
                }
            }
        }
    }
}